import axios from 'axios'

export default {
  fetchCollections ({ commit }) {
    commit('incrementGetCollectionsCallCounter')
    return axios.get(process.env.API_BASE_URL + '/collection/list', {
      params: {
        offset: 0,
        limit: 20
      }
    })
      .then(response => {
        commit('addCollections', { collections: response.data })
        commit('incrementGetCollectionsLoadOffset', 20)
      })
  },
  fetchMetaInfo ({ commit }, route) {
    return axios.post(process.env.API_BASE_URL + '/meta_info/fetch', {
      route_name: route.name,
      route_params: route.params
    })
      .then(response => {
        commit('setMetaInfo', response.data)
      })
  }
}
