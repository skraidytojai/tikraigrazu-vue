import axios from 'axios'

export const KEY_TKG_COLLECTIONS = 'tkg_collections'
export const KEY_TKG_CHECKOUT_FORM = 'tkg_checkout_form'

// For testing. Commented out because of: 1. SSR, 2. we don't have tests yet
// if (navigator.userAgent.indexOf('PhantomJS') > -1) {
//  window.localStorage.clear()
// }

const initialCheckoutForm = {
  buyer_name: '',
  buyer_email: '',
  buyer_phone: '',
  selected_shipping_type: 'pickup',
  shipping_name: '',
  shipping_address: '',
  shipping_city: '',
  shipping_post_code: '',
  shipping_country: 'Lietuva',
  selected_product: null, // for UI
  selected_product_type: 'canvas_natural',
  selected_qty: 1,
  picture_id: null
}

export const state = function () {
  return {
    collections: JSON.parse(typeof window !== 'undefined' && window.localStorage.getItem(KEY_TKG_COLLECTIONS) ? window.localStorage.getItem(KEY_TKG_COLLECTIONS) : '[]'),
    collections_load_offset: 0,
    collections_single_load_limit: 20,
    get_collections_call_counter: 0,
    total_collection_count: 0, // unused?
    is_view_picture_modal_opened: false,
    view_collection: {
      collection: null,
      pictures: [],
      current_load_offset: 0
    },
    checkout_form: typeof window !== 'undefined' && window.localStorage.getItem(KEY_TKG_CHECKOUT_FORM) ? JSON.parse(window.localStorage.getItem(KEY_TKG_CHECKOUT_FORM)) : initialCheckoutForm,
    meta_info: {
      title: '',
      description: '',
      'og:description': '',
      type: '',
      'og:image': ''
    }
  }
}

export const mutations = {
  addCollections (state, { collections }) {
    // new function, for ssr
    state.collections.push(...collections)
  },
  getCollections (state, loaderCallback) {
    state.get_collections_call_counter++
    axios.get(process.env.API_BASE_URL + '/collection/list', {
      params: {
        offset: state.collections_load_offset,
        limit: state.collections_single_load_limit
      }
    })
      .then(response => {
        state.collections.push(...response.data)
        if (loaderCallback) {
          loaderCallback(response.data.length)
        }
      })
      .catch(e => {
        // this.errors.push(e)
        if (loaderCallback) {
          loaderCallback(e)
        }
      })
    state.collections_load_offset += state.collections_single_load_limit
  },
  purgeCollections (state) {
    state.collections = []
  },
  openViewPictureModal (state) {
    state.is_view_picture_modal_opened = true
  },
  closeViewPictureModal (state) {
    state.is_view_picture_modal_opened = false
  },
  resetViewCollectionState (state) {
    state.view_collection.collection = null
    state.view_collection.pictures = []
    state.view_collection.current_load_offset = 0
  },
  viewCollectionAddPictures (state, pictures) {
    state.view_collection.pictures.push(...pictures)
  },
  incrementViewCollectionCurrentLoadOffset (state, incrementBy) {
    state.view_collection.current_load_offset += incrementBy
  },
  updateCheckoutForm (state, checkoutForm) {
    state.checkout_form = checkoutForm
  },
  incrementGetCollectionsCallCounter (state) {
    state.get_collections_call_counter++
  },
  incrementGetCollectionsLoadOffset (state, incrementBy) {
    state.collections_load_offset += incrementBy
  },
  setMetaInfo (state, metaInfo) {
    state.meta_info = metaInfo
  },
  resetMetaInfo (state) {
    state.meta_info = {
      title: 'Kruopščiai atrinktos drobės',
      description: 'Kruopščiai atrinktos drobės',
      'og:description': 'Kruopščiai atrinktos drobės',
      type: 'business.business',
      'og:url': 'https://www.' + process.env.BASE_DOMAIN,
      'og:image': 'https://www.' + process.env.BASE_DOMAIN + '/static/hero/hero2_768_opt.jpg'}
  }
}
