import Vue from 'vue'
import Vuex from 'vuex'
import { state, mutations } from './mutations'
import actions from './actions'
import plugins from './plugins'
// import actions from './actions'

Vue.use(Vuex)

export function createStore () {
  return new Vuex.Store({
    state,
    mutations,
    actions,
    plugins
  })
}
