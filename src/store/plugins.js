import { KEY_TKG_COLLECTIONS, KEY_TKG_CHECKOUT_FORM } from './mutations'

const localStoragePlugin = store => {
  store.subscribe((mutation, { collections }) => {
    if (typeof window !== 'undefined') {
      window.localStorage.setItem(KEY_TKG_COLLECTIONS, JSON.stringify(collections))
    }
  })
  store.subscribe((mutation, { checkout_form }) => {
    if (typeof window !== 'undefined') {
      window.localStorage.setItem(KEY_TKG_CHECKOUT_FORM, JSON.stringify(checkout_form))
    }
  })
}

export default [localStoragePlugin]
