import Vue from 'vue'
import Router from 'vue-router'
import Meta from 'vue-meta'
import { createStore } from '../store/index'
Vue.use(Router)
Vue.use(Meta)

export function createRouter () {
  const router = new Router({
    mode: 'history',
    routes: [
      {
        path: '/',
        name: 'Home',
        component: () => import('../components/Home.vue')
      },
      {
        path: '/kainos',
        name: 'Pricing',
        component: () => import('../components/static_pages/PricingPage.vue')
      },
      {
        path: '/apie-mus',
        name: 'AboutUsPage',
        component: () => import('../components/static_pages/AboutUsPage.vue')
      },
      {
        path: '/laimek-drobe',
        name: 'LaimekDrobePage',
        component: () => import('../components/static_pages/LaimekDrobePage.vue')
      },
      {
        path: '/kolekcija/:slug',
        name: 'ViewCollection',
        component: () => import('../components/ViewCollection.vue'),
        props: true
      },
      {
        path: '/foto/:id/:slug',
        name: 'ViewPicture',
        component: () => import('../components/ViewPicture.vue'),
        props: true
      },
      {
        path: '/pirkti/:id/:slug',
        name: 'BuyPicture',
        component: () => import('../components/BuyPicture.vue'),
        props: true
      },
      {
        path: '/aciu',
        name: 'PaymentThankYouPage',
        component: () => import('../components/static_pages/PaymentThankYouPage.vue')
      },
      {
        path: '/klaida',
        name: 'PaymentErrorPage',
        component: () => import('../components/static_pages/PaymentErrorPage.vue')
      },
      {
        path: '/taisykles',
        name: 'TermsPage',
        component: () => import('../components/static_pages/TermsPage.vue')
      },
      {
        path: '/laikikliai-drobems-kabink-be-grazto',
        name: 'HangingInstructionsPage',
        component: () => import('../components/static_pages/HangingInstructionsPage.vue')
      },
      {
        path: '/kokybes-palyginimas',
        name: 'QualityComparisonPage',
        component: () => import('../components/static_pages/QualityComparisonPage.vue')
      },
      {
        path: '/spauda-ant-drobes-su-jusu-nuotrauka',
        name: 'PrintWithYourPicturePage',
        component: () => import('../components/static_pages/PrintWithYourPicturePage.vue')
      }
    ],
    scrollBehavior (to, from, savedPosition) {
      if ((to.name === 'ViewCollection' && savedPosition) || (to.name === 'Home' && savedPosition)) {
        return savedPosition
      } else {
        return { x: 0, y: 0 }
      }
    }
  })
  /* const store = createStore()
  router.beforeEach((to, from, next) => {
    store.commit('resetMetaInfo')
    next()
  }) */
  return router
}
