import { createApp } from './main'
import Vue from 'vue'
import vueScrollTo from 'vue-scroll-to'
import SocialSharing from 'vue-social-sharing'
import VueAnalytics from 'vue-analytics'

Vue.use(vueScrollTo)
Vue.use(SocialSharing)

Vue.mixin({
  beforeRouteUpdate (to, from, next) {
    const { asyncData } = this.$options
    if (asyncData) {
      asyncData({
        store: this.$store,
        route: to
      }).then(next).catch(next)
    } else {
      next()
    }
  }
})

const { app, router, store } = createApp()
if (window.__INITIAL_STATE__) {
  store.replaceState(window.__INITIAL_STATE__)
}

Vue.use(VueAnalytics, {
  id: 'UA-114661942-1',
  router
})

// this assumes App.vue template root element has `id="app"`
router.onReady(() => {
  // 1st strategy needed? https://ssr.vuejs.org/en/data.html
  app.$mount('#app')
})
