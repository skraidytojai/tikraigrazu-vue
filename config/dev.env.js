'use strict'
const merge = require('webpack-merge')
const prodEnv = require('./prod.env')

module.exports = merge(prodEnv, {
  NODE_ENV: '"development"',
  API_BASE_URL: '"http://app.tikraigrazu.localhost/api"',
  STATIC_BASE_URL: '"http://static.tikraigrazu.localhost"',
  BUSINESS_PHONE_NUMBER: '"+370 630 90709"',
  BUSINESS_EMAIL: '"labas@tikraigrazu.lt"',
  BASE_DOMAIN: '"tikraigrazu.localhost"',
  APP_BASE_URL: '"http://app.tikraigrazu.localhost"',
})
