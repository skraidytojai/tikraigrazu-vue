'use strict'
module.exports = {
  NODE_ENV: '"production"',
  API_BASE_URL: '"https://app.tikraigrazu.lt/api"',
  STATIC_BASE_URL: '"https://static.tikraigrazu.lt"',
  BUSINESS_PHONE_NUMBER: '"+370 630 90709"',
  BUSINESS_EMAIL: '"labas@tikraigrazu.lt"',
  BASE_DOMAIN: '"tikraigrazu.lt"',
  APP_BASE_URL: '"https://app.tikraigrazu.lt"',
}
